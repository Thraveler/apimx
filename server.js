let movimientos_v2 = require('./movimientos-v2.json');
let bodyParser = require('body-parser');

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(bodyParser.json())
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', (req, res) => {
  res.json({
    message: "Hi there, people!"
  });
});

app.post('/', (req, res) => {
  res.json({
    message: "This is a POST method"
  });
});

app.get('/file', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.delete('/', (req, res) => {
  res.json({
    message: "This is a DELETE method"
  });
});

app.get('/Clientes/:idCliente', (req, res) => {
  res.json({
    message: "This is a GET method with idCliente " + req.params.idCliente
  });
});

app.get('/v1/Movimientos', (req, res) => {
  res.sendFile(path.join(__dirname, 'movimientos-v1.json'));
});

app.get('/v2/Movimientos', (req, res) => {
  res.json(movimientos_v2);
});

app.get('/v2/Movimientos/:idMovimiento', (req, res) => {
  res.json(movimientos_v2[req.params.idMovimiento - 1]);
});

app.get('/v2/movimientosQuery', (req, res) => {
  res.json(req.query);
});

app.post('/v2/Movimientos', (req, res) => {
  let nuevo = req.body;
  nuevo.id = movimientos_v2.length + 1;

  movimientos_v2.push(nuevo);
  res.json(movimientos_v2);
});

app.put('/', (req, res) => {
  res.json({message: "Peticion put recibida"});
});

